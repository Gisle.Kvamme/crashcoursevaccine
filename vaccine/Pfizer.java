package vaccines;

import java.time.LocalDate;

public class Pfizer extends Vaccine {

    public Pfizer(LocalDate deliveryDate) {
        super(deliveryDate);
    }

    @Override
    public String getName() {
        return "Pfizer";
    }
}
