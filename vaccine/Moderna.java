package vaccines;

import java.time.LocalDate;

public class Moderna extends Vaccine {

    public Moderna(LocalDate deliveryDate) {
        super(deliveryDate);
    }

    @Override
    public String getName() {
        return "Moderna";
    }
}
