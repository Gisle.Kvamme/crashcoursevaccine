public class Patient implements Comparable<Patient> {
    private String name;
    private UnderlyingConditionGrade underlyingConditionGrade;
    private int age;

    public Patient(String name, int age, UnderlyingConditionGrade underlyingCondition) {
        this.name = name;
        this.age = age;
        this.underlyingConditionGrade = underlyingCondition;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Patient o) {
        /* int ourCondition = this.underlyingConditionGrade.getValue();
        int theirCondition = o.underlyingConditionGrade.getValue();
        if (ourCondition > theirCondition) {
            return -1;
        } else if (ourCondition < theirCondition) {
            return 1;
        } else {
            if (this.age > o.age)
                return -1;
            else if (this.age < o.age)
                return 1;
            else
                return 0;
        } */

        int ourCondition = this.underlyingConditionGrade.getValue();
        int theirCondition = o.underlyingConditionGrade.getValue();

        if (ourCondition != theirCondition)
            return Integer.compare(theirCondition, ourCondition);
        else
            return Integer.compare(o.age, this.age);
    }
}
